# Travail Pratique: Pompe à essence

## Description du projet

L’accommodation "BonService" vient de faire installer une pompe à essence pour augmenter l’achalandage de son commerce.

Votre tâche générale consiste à mettre en place un programme d’affichage d’achat de carburant.

Ce système comprend :

- Un afficheur 4 digits (de 7-segments) qui montre la quantité d’essence ou le prix à payer. Toutes les valeurs sont arrondies au centième.
- Un site web permettant de modifier le prix du litre d’essence
- Pour accéder au site web, votre système doit servir de point d’accès
- Le prix du litre est sauvegardé dans un fichier au format JSON sur la mémoire flash de votre système
- Un bouton poussoir servant à simuler le pistolet de la pompe
- Un bouton poussoir servant à simuler le paiement et la fin de la transaction (rôle du préposé aux pompes)

### Procédures pour la clientèle

À l’arrivée d’un client devant le distributeur, les valeurs sont évidemment « 00.00 ». On affiche toujours deux chiffres avant le point et deux après. Dans votre cas, le point décimal sera simulé par les « : » présents entre les digits 2 et 3.

Quand le pistolet de la pompe est actionné (bouton appuyé), le remplissage débute : le prix s’affiche et augmente dépendamment du prix du litre. Le volume d’essence s’incrémente de 23 litres par minute.
Quand le pistolet de la pompe est relâché, le remplissage cesse.

Après 5 secondes de repos du pistolet (bouton relâché), l’affichage montre, en alternance, le prix à payer et la quantité d’essence. La fréquence d’affichage est de 1000 millisecondes.

Afin de simplifier le problème, le remplissage s’arrête quand le premier entre le volume ou le prix atteint 99.99.

Lorsque le bouton est appuyé à nouveau, le cycle de remplissage se poursuit.

### Procédures pour la gestion des pompes

Lorsque le client a terminé, le préposé aux pompes remet le tout à ZÉRO. Cette action sera simulée par le deuxième bouton indiquant la fin de transaction. La pompe sera prête pour le prochain client.

### Modification du prix du litre

Une interface web est offerte au préposé pour modifier le prix à la pompe. A l’aide de son téléphone cellulaire, il se connecte au réseau offert par la pompe. Il peut ensuite consulter le prix du litre affiché à la pompe ou le modifier.

Si aucun prix n’a été enregistré dans votre système, la pompe doit afficher « 88:88 » et ne pas permettre le service afin de forcer le préposer à indiquer le prix du litre.

(Optionnel : cette dernière action ne peut pas se faire si la pompe est en cours d'utilisation par un client + 5%)
