#pragma once
#include <Arduino.h>
#include "Bouton.h"
#include "Action.h"

class BoutonPistolet : public Bouton {
    private:
        uint8_t m_pin;
        Action* m_actionBouton;
        int m_dernierEtatBouton;
        long m_derniereDateChangement;
        int m_dernierEtatStableBouton;
        const int m_delaiMinPression = 25;

    public:
        BoutonPistolet(int p_pinBouton, Action* p_actionBouton);
        virtual void tick();
};
