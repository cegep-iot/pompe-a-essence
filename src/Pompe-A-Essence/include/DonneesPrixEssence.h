#pragma once
#include <Arduino.h>

class DonneesPrixEssence{
    private:
        bool m_prixChanger;
        bool m_pompeEstUtilise;
    
    public: 
        DonneesPrixEssence();
        String obtenirPrixEssence();
        bool modifierPrixEssence(String p_nouveauTarif);
        bool verifierNouveauPrix();
        bool set_pompeEstUtilise(bool p_estUtilise);
};
