#pragma once
#include <Arduino.h>
#include "Action.h"
#include "Affichage4Digits.h"

class ActionPistolet : public Action {
    private:
        Affichage4Digits* m_affichage4Digits;

    public:
        ActionPistolet(Affichage4Digits* p_affichage4Digits);
        virtual void executer();
};
