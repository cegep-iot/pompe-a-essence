#pragma once
#include <Arduino.h>
#include <FS.h>
#include "DonneesPrixEssence.h"

class WebServer;

class ServeurWeb {
 public:
  ServeurWeb(DonneesPrixEssence* p_donneesPrix);
  void tick();
  void set_pompeEstUtilise(bool p_bool);

 private:
  WebServer* m_webServer;
  DonneesPrixEssence* m_donnesPrixEssence;
  bool m_pompeEstUtilise;

  void obtenirPrixEssence();
  void modifierPrixEssence();
  void afficherRacine();

  void ajouterFichiersStatiques(String const& p_debutNomFichier);
  void ajouterFichiersStatiques(String const& p_debutNomFichier,
                                File& p_fichier);
  void optionsCors() const;
  void envoyerCors() const;
  void ressourceNonTrouvee(String const& p_nomFichier);
};
