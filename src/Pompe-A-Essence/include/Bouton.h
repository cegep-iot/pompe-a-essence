#pragma once
#include <Arduino.h>
#include "Action.h"

class Bouton {
    public:
        virtual void tick() = 0;
};
