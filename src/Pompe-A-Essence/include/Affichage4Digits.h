#pragma once
#include <Arduino.h>
#include "Affichage4DigitsProxy.h"

struct Digits {
    uint8_t tableauDigits[4];
};

class Affichage4Digits {
    private:
        Affichage4DigitsProxy* m_proxy;

        const double m_debitQuantite = 0.000384;
        const float m_limiteAffichage = 99.99;
        const long m_frequenceAffichage = 1000;
        const long m_delaiArreteAvantAffichage = 5000;

        enum Affichage {
            PRIX,
            QUANTITE
        };

        Affichage m_estAffiche;

        long m_dateDernierChangement;
        long m_dateArret;

        bool m_estArrete;
        bool m_utilisationEnCours;

        float m_tarif;
        float m_prix;
        float m_quantite;

        void afficher(Digits* p_digits);
        byte valeurSegment(int p_valeur);
        Digits* decomposerNombre(float p_valeur);
        void afficherPrix();
        void afficherQuantite();
        void verifierLimites();
        void arreter();
        void demarrer();

    public:
        Affichage4Digits(Affichage4DigitsProxy* p_proxy);
        void modifierTarif(float p_tarif);
        void remettreAZero();
        bool estUtilisee();
        void arreterDemarrer();
        void tick();
};
