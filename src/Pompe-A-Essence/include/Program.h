#pragma once
#include <Arduino.h>
#include "AccessPoint.h"
#include "Bouton.h"
#include "Affichage4Digits.h"
#include "DonneesPrixEssence.h"
#include "ServeurWeb.h"

class Program {
    private:
        AccessPoint* m_accessPoint;
        ServeurWeb* m_serveurWeb;
        DonneesPrixEssence* m_donneesPrixEssence;
        Bouton* m_boutonPistolet;
        Bouton* m_boutonPaiement;
        Affichage4Digits* m_affichage4Digits;

        void verificationTarif();
        void verificationUtilisation();

    public:
        Program();
        void loop();
};
