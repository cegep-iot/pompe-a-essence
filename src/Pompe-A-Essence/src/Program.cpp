#include <Arduino.h>
#include "Program.h"
#include "AccessPoint.h"
#include "Bouton.h"
#include "BoutonPistolet.h"
#include "BoutonPaiement.h"
#include "Action.h"
#include "ActionPistolet.h"
#include "ActionPaiement.h"
#include "Affichage4DigitsProxyTM1637.h"
#include "Affichage4Digits.h"
#include "ServeurWeb.h"

Program::Program()
{
    // Pins pour 4 digits:
    const uint8_t pinHorloge = 26;
    const uint8_t pinDonnees = 25;

    // Pins pour boutons:
    const uint8_t pinBoutonPistolet = 17;
    const uint8_t pinBoutonPaiement = 16;

    AccessPoint* accessPoint = new AccessPoint();
    this->m_accessPoint = accessPoint;
    this->m_accessPoint->setup();

    DonneesPrixEssence* DPE = new DonneesPrixEssence();
    this->m_donneesPrixEssence = DPE;

    ServeurWeb* serveurWeb = new ServeurWeb(DPE);
    this->m_serveurWeb = serveurWeb;

    Affichage4DigitsProxy* affichage4DigitsProxy = new Affichage4DigitsProxyTM1637(pinHorloge, pinDonnees);
    Affichage4Digits* affichage4Digits = new Affichage4Digits(affichage4DigitsProxy);
    this->m_affichage4Digits = affichage4Digits;
    
    Action* actionPistolet = new ActionPistolet(affichage4Digits);
    Bouton* boutonPistolet = new BoutonPistolet(pinBoutonPistolet, actionPistolet);
    this->m_boutonPistolet = boutonPistolet;

    Action* actionPaiement = new ActionPaiement(affichage4Digits);
    Bouton* boutonPaiement = new BoutonPaiement(pinBoutonPaiement, actionPaiement);
    this->m_boutonPaiement = boutonPaiement;

    String nouveauTarif = this->m_donneesPrixEssence->obtenirPrixEssence();
    this->m_affichage4Digits->modifierTarif(nouveauTarif.toFloat());
}

void Program::verificationUtilisation(){
    bool estUtilise = this->m_affichage4Digits->estUtilisee();
    this->m_donneesPrixEssence->set_pompeEstUtilise(estUtilise);
    this->m_serveurWeb->set_pompeEstUtilise(estUtilise);
}

void Program::verificationTarif(){
    if (this->m_donneesPrixEssence->verifierNouveauPrix()){
        String nouveauTarif = this->m_donneesPrixEssence->obtenirPrixEssence();
        this->m_affichage4Digits->modifierTarif(nouveauTarif.toFloat());
    }
}

void Program::loop()
{
    this->m_serveurWeb->tick();

    this->verificationUtilisation();
    this->verificationTarif();

    this->m_affichage4Digits->tick();
    this->m_boutonPistolet->tick();
    this->m_boutonPaiement->tick();
}
