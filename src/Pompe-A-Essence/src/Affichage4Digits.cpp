#include <Arduino.h>
#include "Affichage4Digits.h"
#include "Affichage4DigitsProxy.h"

Affichage4Digits::Affichage4Digits(Affichage4DigitsProxy* p_proxy): m_proxy(p_proxy)
{
    long dateActuelle = millis();
    this->m_dateDernierChangement = dateActuelle;
    this->m_dateArret = dateActuelle;

    this->m_estArrete = true;
    this->m_utilisationEnCours = false;
    this->m_estAffiche = PRIX;

    this->m_prix = 0.0;
    this->m_quantite = 0.0;
    this->m_tarif = NULL;
}

void Affichage4Digits::afficher(Digits* p_digits)
{
    this->m_proxy->afficher(p_digits->tableauDigits[0], p_digits->tableauDigits[1], p_digits->tableauDigits[2], p_digits->tableauDigits[3]);

    delete p_digits;
    p_digits = NULL;
}

byte Affichage4Digits::valeurSegment(int p_valeur)
{
    byte caracteres[10] = {
        0b00111111,
        0b00000110,
        0b01011011,
        0b01001111,
        0b01100110,
        0b01101101,
        0b01111101,
        0b00000111,
        0b01111111,
        0b01101111
    };

    return caracteres[p_valeur];
}

Digits* Affichage4Digits::decomposerNombre(float p_valeur)
{
    Digits* digits = new Digits();

    int valeurEntiere = int(floor(p_valeur));
    int valeurDecimale = int(floor((p_valeur - valeurEntiere) * 100));

    digits->tableauDigits[0] = this->valeurSegment((valeurEntiere / 10) % 10);
    digits->tableauDigits[1] = this->valeurSegment(valeurEntiere % 10);
    digits->tableauDigits[2] = this->valeurSegment((valeurDecimale / 10) % 10);
    digits->tableauDigits[3] = this->valeurSegment(valeurDecimale % 10);

    return digits;
}

void Affichage4Digits::afficherPrix()
{
    Digits* digits = this->decomposerNombre(this->m_prix);
    this->afficher(digits);
}

void Affichage4Digits::afficherQuantite()
{
    Digits* digits = this->decomposerNombre(this->m_quantite);
    this->afficher(digits);
}

void Affichage4Digits::verifierLimites()
{
    if(this->m_quantite >= 99.99 || this->m_prix >= 99.99)
    {
        this->m_estArrete = true;
    }
}

void Affichage4Digits::arreter()
{
    this->m_estArrete = true;

    long dateActuelle = millis();
    this->m_dateDernierChangement = dateActuelle;
    this->m_dateArret = dateActuelle;
}

void Affichage4Digits::demarrer()
{
    this->m_utilisationEnCours = true;
    this->m_estArrete = false;

    this->m_dateDernierChangement = millis();
}

void Affichage4Digits::modifierTarif(float p_tarif)
{
    if(!this->m_utilisationEnCours)
    {
        this->m_estArrete = true;
        this->m_dateDernierChangement = millis();
        this->m_tarif = p_tarif;
    }
}

void Affichage4Digits::remettreAZero()
{
    this->m_dateDernierChangement = millis();

    this->m_utilisationEnCours = false;
    this->m_estArrete = true;

    this->m_prix = 0.0;
    this->m_quantite = 0.0;
    this->m_estAffiche = PRIX;

    this->afficherPrix();
}

bool Affichage4Digits::estUtilisee()
{
    return this->m_utilisationEnCours;
}

void Affichage4Digits::arreterDemarrer()
{
    if(this->m_estArrete)
    {
        this->demarrer();
    }
    else
    {
        this->arreter();
    }
}

void Affichage4Digits::tick()
{
    long dateActuelle = millis();

    if(this->m_tarif != NULL)
    {
        if(this->m_estArrete)
        {
            if(dateActuelle - this->m_dateArret > this->m_delaiArreteAvantAffichage)
            {
                if(dateActuelle - this->m_dateDernierChangement > this->m_frequenceAffichage)
                {
                    if(this->m_estAffiche == QUANTITE)
                    {
                        this->afficherPrix();
                        this->m_estAffiche = PRIX;
                    }
                    else
                    {
                        this->afficherQuantite();
                        this->m_estAffiche = QUANTITE;
                    }

                    this->m_dateDernierChangement = dateActuelle;
                }
            }
        }
        else
        {
            this->m_quantite = m_quantite + ((dateActuelle - this->m_dateDernierChangement) * this->m_debitQuantite);
            this->m_prix = this->m_quantite * this->m_tarif;

            this->verifierLimites();
            this->afficherPrix();

            if(this->m_estAffiche == QUANTITE)
            {
                this->m_estAffiche = PRIX;
            }

            this->m_dateDernierChangement = dateActuelle;
        }
    }
    else
    {
        this->m_proxy->afficher(0b01111111, 0b01111111, 0b01111111, 0b01111111);
    }
}
