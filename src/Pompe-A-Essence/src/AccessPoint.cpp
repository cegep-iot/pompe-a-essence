#include <WiFi.h>
#include "AccessPoint.h"

    IPAddress adresseIPInterne(192, 168, 23, 1);
    IPAddress passerelle(192, 168, 23, 1);
    IPAddress masqueReseau(255, 255, 255, 0);

void AccessPoint::setup(){
        bool configReussie = WiFi.softAPConfig(adresseIPInterne, passerelle, masqueReseau);

        bool demarrageAPReussi = false;
        Serial.println(String("Configuration réseau du point d'accès : ") +
                (configReussie ? "Réussie" : "Échec !"));

    if (configReussie) {
        demarrageAPReussi = WiFi.softAP(this->SSID, this->motPasse);
        Serial.println(String("Démarrage du point d'accès : ") +
                    (demarrageAPReussi ? "Réussi" : "Échec !"));

        if (demarrageAPReussi) {
        Serial.print("Adresse IP du point d'accès : ");
        Serial.println(WiFi.softAPIP());

        }
    }

    this->erreur = !(configReussie && demarrageAPReussi);
}
