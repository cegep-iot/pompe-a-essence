#include <Arduino.h>
#include "Bouton.h"
#include "BoutonPistolet.h"
#include "Action.h"

BoutonPistolet::BoutonPistolet(int p_pinBouton, Action* p_actionBouton) : m_pin(p_pinBouton),
                                                            m_actionBouton(p_actionBouton)
{
    pinMode(this->m_pin, INPUT);
    this->m_dernierEtatBouton = HIGH;
    this->m_derniereDateChangement = 0;
    this->m_dernierEtatStableBouton = HIGH;
}

void BoutonPistolet::tick()
{
    int etatBouton = digitalRead(this->m_pin);
    long dateActuelle = millis();

    if (etatBouton != this->m_dernierEtatBouton)
    {
        this->m_derniereDateChangement = dateActuelle;
        this->m_dernierEtatBouton = etatBouton;
    }

    if(dateActuelle - this->m_derniereDateChangement > this->m_delaiMinPression)
    {
        if (this->m_dernierEtatStableBouton == HIGH && etatBouton == LOW)
        {
            this->m_actionBouton->executer();
        }
        else if (this->m_dernierEtatStableBouton == LOW && etatBouton == HIGH)
        {
            this->m_actionBouton->executer();
        }

        this->m_dernierEtatStableBouton = etatBouton;
    }
}
