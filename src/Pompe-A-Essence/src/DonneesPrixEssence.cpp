#include <Arduino.h>
#include <SPIFFS.h>
#include <Fs.h>
#include <ArduinoJson.h>
#include "DonneesPrixEssence.h"

DonneesPrixEssence::DonneesPrixEssence(){
    this->m_prixChanger = false;
    this->m_pompeEstUtilise = false;
}

// Aide sur: https://techtutorialsx.com/2019/06/02/esp8266-spiffs-reading-a-file/
bool DonneesPrixEssence::modifierPrixEssence(String p_nouveauPrix){
    if(m_pompeEstUtilise){
        Serial.println("DonneesPrixEssence: modification du tarif interdite lorsque la pompe est utilise");
        return false;
    }
    else{
        File file = SPIFFS.open("/tarif.json", FILE_WRITE);
        if(!file){
            Serial.println("Echec dans l'ouverture du fichier");
            return false;
        }

        int bytesEcrit = file.print(p_nouveauPrix);

        if (bytesEcrit > 0){
            Serial.println("Le fichier a ete ecrit");
            this->m_prixChanger = true;
            file.close();
            return true;
        }
        else{
            Serial.println("Echec dans l'ecriture du fichier");
            file.close();
            return false;
        }
    }
}

// Aide sur: https://techtutorialsx.com/2019/06/02/esp8266-spiffs-reading-a-file/
String DonneesPrixEssence::obtenirPrixEssence(){

    File file = SPIFFS.open("/tarif.json", FILE_READ);
    String raw = file.readString();
    DynamicJsonDocument doc(200);
    deserializeJson(doc, raw);
    file.close();

    // Retourner tarif ou raw??
    return doc["tarif"];
}

bool DonneesPrixEssence::verifierNouveauPrix(){
    if(this->m_prixChanger == true)
    {
        this->m_prixChanger = false;
        return true;
    }
    else
    {
        return false;
    }
}

bool DonneesPrixEssence::set_pompeEstUtilise(bool p_estUtilise){
    this->m_pompeEstUtilise = p_estUtilise;
    return this->m_pompeEstUtilise;
}
