#include <Arduino.h>
#include "Bouton.h"
#include "BoutonPaiement.h"
#include "Action.h"

BoutonPaiement::BoutonPaiement(int p_pinBouton, Action* p_actionBouton) : m_pin(p_pinBouton),
                                                            m_actionBouton(p_actionBouton)
{
    pinMode(this->m_pin, INPUT);
    this->m_dernierEtatBouton = HIGH;
    this->m_derniereDateChangement = 0;
    this->m_dernierEtatStableBouton = HIGH;
}

void BoutonPaiement::tick()
{
    int etatBouton = digitalRead(this->m_pin);
    long dateActuelle = millis();

    if (etatBouton != this->m_dernierEtatBouton)
    {
        this->m_derniereDateChangement = dateActuelle;
        this->m_dernierEtatBouton = etatBouton;
    }

    if(dateActuelle - this->m_derniereDateChangement > this->m_delaiMinPression)
    {
        if (this->m_dernierEtatStableBouton == HIGH && etatBouton == LOW)
        {

        }
        else if (this->m_dernierEtatStableBouton == LOW && etatBouton == HIGH)
        {
            this->m_actionBouton->executer();
        }

        this->m_dernierEtatStableBouton = etatBouton;
    }
}
