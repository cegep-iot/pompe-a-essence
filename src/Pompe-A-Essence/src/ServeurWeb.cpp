#include <Arduino.h>
#include <SPIFFS.h>
#include <Fs.h>
#include <WebServer.h>
#include <detail/RequestHandlersImpl.h>
#include <uri/UriRegex.h>
#include <ArduinoJson.h>
#include "ServeurWeb.h"

ServeurWeb::ServeurWeb(DonneesPrixEssence* p_donnesPrixEssence) : m_donnesPrixEssence(p_donnesPrixEssence) {
  SPIFFS.begin();

  WebServer* webServer = new WebServer();
  this->m_webServer = webServer;
  this->m_pompeEstUtilise = false;

  this->ajouterFichiersStatiques("/");

  this->m_webServer->on("/", HTTPMethod::HTTP_GET,
                        [this]() { this->afficherRacine(); });
  this->m_webServer->on(Uri("/prix-essence"), HTTPMethod::HTTP_GET,
                        [this]() { this->obtenirPrixEssence(); });
  this->m_webServer->on(Uri("/prix-essence"), HTTPMethod::HTTP_PUT,
                        [this]() { this->modifierPrixEssence(); });
  this->m_webServer->on(UriRegex(".*"), HTTPMethod::HTTP_OPTIONS,
                        [this]() {
                          this->optionsCors();
                        });  
  this->m_webServer->onNotFound(
                      [this]() { 
                       this->ressourceNonTrouvee(this->m_webServer->uri()); 
                      });

  this->m_webServer->begin();
}

void ServeurWeb::afficherRacine() {
  this->envoyerCors();

  Serial.println("Réception requête: AfficherRacine");
  Serial.println(this->m_webServer->uri());

  this->m_webServer->sendHeader("Location", "index.html", true);
  this->m_webServer->send(301, "text/plain", "");
}

void ServeurWeb::obtenirPrixEssence(){
  this->envoyerCors();

  String res = this->m_donnesPrixEssence->obtenirPrixEssence();

  this->m_webServer->send(200, "text/json", res);
}

void ServeurWeb::modifierPrixEssence(){
  this->envoyerCors();

  if(this->m_pompeEstUtilise){
    this->m_webServer->send(403,"text/plain", "refuse");
  }
  else{
    String json = this->m_webServer->arg("plain");

    bool reussit = this->m_donnesPrixEssence->modifierPrixEssence(json);

    if (reussit == true){
      this->m_webServer->send(200, "text/plain", "Modification Reussit!"); 
    }
    else{
      this->m_webServer->send(500, "text/plain", "Modification Echoue!"); 
    }
  }
}

void ServeurWeb::set_pompeEstUtilise(bool p_estUtilise){
  this->m_pompeEstUtilise = p_estUtilise;
}

void ServeurWeb::tick() 
{ 
  this->m_webServer->handleClient();
}

void ServeurWeb::ajouterFichiersStatiques(String const& p_debutNomFichier) {
  File racine = SPIFFS.open("/");
  ajouterFichiersStatiques(p_debutNomFichier, racine);
}

void ServeurWeb::ajouterFichiersStatiques(String const& p_debutNomFichier,
                                          File& p_repertoire) {
  if (!p_repertoire) return;

  Serial.println(String("Traitement du répertoire: ") + p_repertoire.name());


  File fichier = p_repertoire.openNextFile();
  while (fichier) {
    String nomFichier = String(fichier.name());
    if (fichier.isDirectory()) {
      ajouterFichiersStatiques(p_debutNomFichier, fichier);
    } else {
      if (nomFichier.startsWith(p_debutNomFichier)) {
        Serial.println(String("Ajout du fichier statique: " + nomFichier));
        this->m_webServer->serveStatic(nomFichier.c_str(), SPIFFS,
                                       nomFichier.c_str());
      }
    }
    fichier.close();
    fichier = p_repertoire.openNextFile();
  }

  p_repertoire.close();
}

void ServeurWeb::ressourceNonTrouvee(const String& p_nomRessource) {
  Serial.println("Ressource '" + p_nomRessource + "' non trouvée !");
  this->m_webServer->send(404, "text/plain",
                          "Ressource '" + p_nomRessource + "' non trouvée !");
}

void ServeurWeb::optionsCors() const {
  this->m_webServer->sendHeader("Access-Control-Allow-Origin", "*");
  this->m_webServer->sendHeader("Access-Control-Max-Age", "600");
  this->m_webServer->sendHeader("Access-Control-Allow-Methods", "PUT,POST,GET,OPTIONS");
  this->m_webServer->sendHeader("Access-Control-Allow-Headers", "*");
  this->m_webServer->send(204);
}

void ServeurWeb::envoyerCors() const {
  this->m_webServer->sendHeader("Access-Control-Allow-Origin", "*");
}
